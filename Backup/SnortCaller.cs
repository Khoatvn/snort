using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

// Code based on http://www.codeproject.com/csharp/LaunchProcess.asp

namespace TestProcessCaller
{
	/// <summary>
	/// A simple form to launch a process using ProcessCaller
	/// and display all StdOut and StdErr in a RichTextBox.
	/// </summary>
	/// <remarks>
    /// Special thanks to Chad Christensen for suggestions
    /// on using the RichTextBox.
    /// Note there are a lot of issues with scrolling on a
    /// RichTextBox, depending upon if the cursor (selection point) 
    /// is in the RichTextBox or not, and if it is hidden or not.
    /// I've disabled the RichTextBox tabstop so that the cursor isn't
    /// automatically placed in the text box.
    /// Now setting or unsetting:
    ///    richTextBox1.HideSelection
    /// will affect if the textbox is always repositioned at the bottom
    ///   when new text is entered.
	/// </remarks>
    public class ProgressForm : System.Windows.Forms.Form
    {
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button btnViewAlert;
		private System.Windows.Forms.TextBox snortIDS;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Button btnDeleteIDS;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button btnView;
		private System.Windows.Forms.TextBox alertWindow;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Button btnIDSRule;
		private System.Windows.Forms.Button btnInterface;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox statusIDS;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.ComponentModel.IContainer components;

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProgressForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

		#region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ProgressForm));
			this.button2 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.btnView = new System.Windows.Forms.Button();
			this.btnStart = new System.Windows.Forms.Button();
			this.btnStop = new System.Windows.Forms.Button();
			this.btnInterface = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.btnViewAlert = new System.Windows.Forms.Button();
			this.snortIDS = new System.Windows.Forms.TextBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.btnDeleteIDS = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.alertWindow = new System.Windows.Forms.TextBox();
			this.btnIDSRule = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.statusIDS = new System.Windows.Forms.TextBox();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.groupBox1.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(792, 568);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(88, 23);
			this.button2.TabIndex = 4;
			this.button2.Text = "Exit";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Controls.Add(this.btnView);
			this.groupBox1.Controls.Add(this.btnStart);
			this.groupBox1.Controls.Add(this.btnStop);
			this.groupBox1.Controls.Add(this.btnInterface);
			this.groupBox1.Location = new System.Drawing.Point(8, 40);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(136, 184);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Capture";
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.Lavender;
			this.button3.Location = new System.Drawing.Point(24, 152);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(88, 23);
			this.button3.TabIndex = 6;
			this.button3.Text = "View ARP";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// btnView
			// 
			this.btnView.BackColor = System.Drawing.Color.Lavender;
			this.btnView.Location = new System.Drawing.Point(24, 120);
			this.btnView.Name = "btnView";
			this.btnView.Size = new System.Drawing.Size(88, 23);
			this.btnView.TabIndex = 5;
			this.btnView.Text = "View output";
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// btnStart
			// 
			this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnStart.BackColor = System.Drawing.Color.Lavender;
			this.btnStart.Location = new System.Drawing.Point(24, 48);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(88, 24);
			this.btnStart.TabIndex = 4;
			this.btnStart.Text = "Capture Inter";
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// btnStop
			// 
			this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnStop.BackColor = System.Drawing.Color.Lavender;
			this.btnStop.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnStop.Location = new System.Drawing.Point(24, 80);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(88, 23);
			this.btnStop.TabIndex = 1;
			this.btnStop.Text = "Stop";
			this.btnStop.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnInterface
			// 
			this.btnInterface.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnInterface.BackColor = System.Drawing.Color.Lavender;
			this.btnInterface.Location = new System.Drawing.Point(24, 16);
			this.btnInterface.Name = "btnInterface";
			this.btnInterface.Size = new System.Drawing.Size(88, 23);
			this.btnInterface.TabIndex = 3;
			this.btnInterface.Text = "Show interf";
			this.btnInterface.Click += new System.EventHandler(this.btnInterface_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(728, 16);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(184, 64);
			this.pictureBox1.TabIndex = 10;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem5});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "Exit";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 528);
			this.label1.Name = "label1";
			this.label1.TabIndex = 12;
			this.label1.Text = "Status";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.checkBox2);
			this.groupBox5.Location = new System.Drawing.Point(168, 56);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(704, 232);
			this.groupBox5.TabIndex = 18;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Alert Window (reads from the alert.ids file)";
			// 
			// checkBox2
			// 
			this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox2.Enabled = false;
			this.checkBox2.Location = new System.Drawing.Point(16, 16);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.TabIndex = 0;
			this.checkBox2.Text = "Snort Started:";
			this.checkBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnViewAlert
			// 
			this.btnViewAlert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnViewAlert.BackColor = System.Drawing.Color.Lavender;
			this.btnViewAlert.Location = new System.Drawing.Point(24, 80);
			this.btnViewAlert.Name = "btnViewAlert";
			this.btnViewAlert.Size = new System.Drawing.Size(88, 23);
			this.btnViewAlert.TabIndex = 4;
			this.btnViewAlert.Text = "View alert.ids";
			this.btnViewAlert.Click += new System.EventHandler(this.btnViewAlert_Click);
			// 
			// snortIDS
			// 
			this.snortIDS.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.snortIDS.Location = new System.Drawing.Point(184, 344);
			this.snortIDS.MaxLength = 1000000;
			this.snortIDS.Multiline = true;
			this.snortIDS.Name = "snortIDS";
			this.snortIDS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.snortIDS.Size = new System.Drawing.Size(680, 152);
			this.snortIDS.TabIndex = 19;
			this.snortIDS.Text = "";
			// 
			// groupBox4
			// 
			this.groupBox4.BackColor = System.Drawing.Color.Lavender;
			this.groupBox4.Controls.Add(this.checkBox1);
			this.groupBox4.Location = new System.Drawing.Point(168, 296);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(704, 216);
			this.groupBox4.TabIndex = 20;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Log Window (read from Snort output - if checkbox enabled)";
			// 
			// checkBox1
			// 
			this.checkBox1.BackColor = System.Drawing.Color.Lavender;
			this.checkBox1.Location = new System.Drawing.Point(16, 24);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.TabIndex = 15;
			this.checkBox1.Text = "Log";
			// 
			// btnDeleteIDS
			// 
			this.btnDeleteIDS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDeleteIDS.BackColor = System.Drawing.Color.Lavender;
			this.btnDeleteIDS.Location = new System.Drawing.Point(24, 32);
			this.btnDeleteIDS.Name = "btnDeleteIDS";
			this.btnDeleteIDS.Size = new System.Drawing.Size(88, 23);
			this.btnDeleteIDS.TabIndex = 5;
			this.btnDeleteIDS.Text = "Delete alert.ids";
			this.btnDeleteIDS.Click += new System.EventHandler(this.btnDeleteIDS_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.BackColor = System.Drawing.Color.Lavender;
			this.groupBox3.Controls.Add(this.btnDeleteIDS);
			this.groupBox3.Controls.Add(this.btnViewAlert);
			this.groupBox3.Location = new System.Drawing.Point(8, 384);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(136, 112);
			this.groupBox3.TabIndex = 11;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "IDS alerts";
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.BackColor = System.Drawing.Color.Lavender;
			this.button1.Location = new System.Drawing.Point(16, 64);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(96, 23);
			this.button1.TabIndex = 5;
			this.button1.Text = "View IDS rule";
			this.button1.Click += new System.EventHandler(this.button1_Click_2);
			// 
			// alertWindow
			// 
			this.alertWindow.Location = new System.Drawing.Point(176, 96);
			this.alertWindow.Multiline = true;
			this.alertWindow.Name = "alertWindow";
			this.alertWindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.alertWindow.Size = new System.Drawing.Size(688, 184);
			this.alertWindow.TabIndex = 13;
			this.alertWindow.Text = "";
			// 
			// btnIDSRule
			// 
			this.btnIDSRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnIDSRule.BackColor = System.Drawing.Color.Lavender;
			this.btnIDSRule.Location = new System.Drawing.Point(16, 24);
			this.btnIDSRule.Name = "btnIDSRule";
			this.btnIDSRule.Size = new System.Drawing.Size(96, 23);
			this.btnIDSRule.TabIndex = 4;
			this.btnIDSRule.Text = "Create IDS rule";
			this.btnIDSRule.Click += new System.EventHandler(this.btnIDSRule_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.BackColor = System.Drawing.Color.Lavender;
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Controls.Add(this.btnIDSRule);
			this.groupBox2.Location = new System.Drawing.Point(8, 256);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(136, 100);
			this.groupBox2.TabIndex = 8;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "IDS rules";
			// 
			// statusIDS
			// 
			this.statusIDS.Location = new System.Drawing.Point(88, 528);
			this.statusIDS.Multiline = true;
			this.statusIDS.Name = "statusIDS";
			this.statusIDS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.statusIDS.Size = new System.Drawing.Size(672, 56);
			this.statusIDS.TabIndex = 7;
			this.statusIDS.Text = "";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = -1;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem4});
			this.menuItem3.Text = "Tutorial";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 0;
			this.menuItem4.Text = "View";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// timer1
			// 
			this.timer1.Interval = 500;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 1;
			this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem6,
																					  this.menuItem7});
			this.menuItem5.Text = "Help";
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 0;
			this.menuItem6.Text = "Bill\'s Site";
			this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 1;
			this.menuItem7.Text = "Tutorial";
			// 
			// ProgressForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Lavender;
			this.ClientSize = new System.Drawing.Size(904, 601);
			this.Controls.Add(this.snortIDS);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.alertWindow);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.statusIDS);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox1);
			this.Menu = this.mainMenu1;
			this.Name = "ProgressForm";
			this.Text = "Snort Capture - Napier University";
			this.Load += new System.EventHandler(this.ProgressForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

        #endregion


        private ProcessCaller processCaller;

		public void runShort(string arguments)
		{
			processCaller = new ProcessCaller(this);
			//processCaller.FileName = @"..\..\hello.bat";
			processCaller.FileName = @"c:\snort\bin\snort.exe";
			processCaller.Arguments = arguments;
			processCaller.StdErrReceived += new DataReceivedHandler(writeStreamInfo);
			processCaller.StdOutReceived += new DataReceivedHandler(writeStreamInfo);
			processCaller.Completed += new EventHandler(processCompletedOrCanceled);
			processCaller.Cancelled += new EventHandler(processCompletedOrCanceled);
			// processCaller.Failed += no event handler for this one, yet.
            
			statusIDS.Text+="Started function." + Environment.NewLine;

			// the following function starts a process and returns immediately,
			// thus allowing the form to stay responsive.
			processCaller.Start();  
		}


        private void btnCancel_Click(object sender, System.EventArgs e)
        {
			checkBox2.Checked=false;
            if (processCaller != null)
            {
                processCaller.Cancel();
            }
			System.Diagnostics.Process[] p =System.Diagnostics.Process.GetProcesses();

			for(int i=0 ;i<p.GetLength(0);i++)
			{
				if (p[i].ProcessName=="snort") p[i].Kill();
			}
			statusIDS.Text+="IDS stopped\r\n";
			btnStart.Enabled=true;
			timer1.Enabled=false;
        }

        /// <summary>
        /// Handles the events of StdErrReceived and StdOutReceived.
        /// </summary>
        /// <remarks>
        /// If stderr were handled in a separate function, it could possibly
        /// be displayed in red in the richText box, but that is beyond 
        /// the scope of this demo.
        /// </remarks>
        private void writeStreamInfo(object sender, DataReceivedEventArgs e)
        {
			if (this.checkBox1.Checked)
			{
				this.snortIDS.AppendText(e.Text + Environment.NewLine);
				if (snortIDS.TextLength>=snortIDS.MaxLength) snortIDS.Clear();
			}
        }

        /// <summary>
        /// Handles the events of processCompleted & processCanceled
        /// </summary>
        private void processCompletedOrCanceled(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
            this.btnStart.Enabled = true;
        }


        [STAThread]
        static void Main(string[] args)         
        {
            Application.Run(new ProgressForm());
        }

		private void ProgressForm_Load(object sender, System.EventArgs e)
		{
			// Added by Bill, to kill old Snort processes...
			System.Diagnostics.Process[] p =System.Diagnostics.Process.GetProcesses();

			for(int i=0 ;i<p.GetLength(0);i++)
			{
				if (p[i].ProcessName=="snort") p[i].Kill();
			}


		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			this.runShort("-W");

		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			if (processCaller != null)
			{
				processCaller.Cancel();
			}
			Application.Exit();
		}



		private void btnStart_Click(object sender, System.EventArgs e)
		{
			if (!Directory.Exists("c:\\bill"))	Directory.CreateDirectory("c:\\bill");

			this.runShort("-dev -i 3 -p -l c:\\bill -K ascii -c c:\\snort\\bin\\napier.txt");
			if (File.Exists("c:\\bill\\alert.ids")) File.Delete("c:\\bill\\alert.ids");
			statusIDS.Text+="Snort started\r\n";
			btnStart.Enabled=false;
			checkBox2.Checked=true;
			alertWindow.Text="";
			timer1.Enabled=true;
			snortIDS.Text="";
		}

		private void btnInterface_Click(object sender, System.EventArgs e)
		{
			checkBox1.Checked=true;
			this.runShort("-W");

		}

		private void btnView_Click(object sender, System.EventArgs e)
		{
			openFileDialog1.InitialDirectory="c:\\bill";
			openFileDialog1.ShowDialog();
			Process.Start("wordpad.exe", openFileDialog1.FileName);	

		}

		private void btnIDSRule_Click(object sender, System.EventArgs e)
		{
			string rule;

			rule = "alert tcp any 80 -> any any (content:\"John\"; content:\"Napier\"; msg:\"John Napier detected\";)";
//			rule = "alert tcp any any -> any 1234 (msg:\"Port 1863 detected\";)";

			StreamWriter SW;
			SW=File.CreateText("c:\\snort\\bin\\napier.txt");
			SW.WriteLine(rule);
			SW.Close();
			statusIDS.Text+="IDS updated... please restart Snort\r\n";
		}

		private void btnViewAlert_Click(object sender, System.EventArgs e)
		{
			if (File.Exists("c:\\bill\\alert.ids"))
			{
				string txt="";
				if (File.Exists("c:\\bill\\test.ids")) File.Delete("c:\\bill\\test.ids");

				File.Copy("c:\\bill\\alert.ids","c:\\bill\\test.ids");
				StreamReader SW = new StreamReader("c:\\bill\\test.ids");
				while ((txt=SW.ReadLine())!=null)
				{
					statusIDS.Text+=txt+"\r\n";
				}
				SW.Close();
				Process.Start("wordpad.exe", "c:\\bill\\test.ids");	

			}
			else statusIDS.Text+="File does not exist...\r\n";
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			Process.Start("IExplore.exe", "http://buchananweb.co.uk/labs_complete_additional_2.pdf");

		}

		private void btnDeleteIDS_Click(object sender, System.EventArgs e)
		{
			if (File.Exists("c:\\bill\\alert.ids"))
			{
				File.Delete("c:\\bill\\alert.ids");	
				statusIDS.Text+="File deleted\r\n";
			}
			if (File.Exists("c:\\bill\\test.ids"))
			{
				File.Delete("c:\\bill\\test.ids");	
				statusIDS.Text+="File deleted\r\n";
			}
			else statusIDS.Text+="File does not exist... so cannot be deleted\r\n";
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if (File.Exists("c:\\bill\\alert.ids"))
			{
				string txt="";
				alertWindow.Text="";
				if (File.Exists("c:\\bill\\test.ids")) File.Delete("c:\\bill\\test.ids");

				File.Copy("c:\\bill\\alert.ids","c:\\bill\\test.ids");
				StreamReader SW = new StreamReader("c:\\bill\\test.ids");
				while ((txt=SW.ReadLine())!=null)
				{
					alertWindow.Text+=txt+"\r\n";
				}
				SW.Close();
				if (checkBox2.Checked==true) statusIDS.Text+=".";
				alertWindow.Focus();
			}
		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
			Process.Start("wordpad.exe", "bill.txt");	

		}

		private void button1_Click_2(object sender, System.EventArgs e)
		{
			Process.Start("wordpad.exe", "c:\\snort\\bin\\napier.txt");	

		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			Process.Start("wordpad.exe", "c:\\bill\\ARP");	
		}

		private void pictureBox1_Click(object sender, System.EventArgs e)
		{
			Process.Start("IExplore.exe", "http://www.napier.ac.uk");
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			Process.Start("IExplore.exe", "http://www.buchananweb.co.uk");

		}

    }
}
